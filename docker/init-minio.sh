#!/bin/bash

sleep 10

mc config host add minio http://minio:9000 minioadmin minioadmin;

if ! mc ls minio/museum;
then
  mc mb minio/museum
fi