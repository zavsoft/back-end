package net.zavsoft.museum.keycloaksecurity;

import java.util.UUID;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {

    private final UserServiceClient userServiceClient;

    @Override
    public boolean hasAuthority(UUID ownerId) {
        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            return false;
        }
        var userId = SecurityContextHolder.getContext().getAuthentication().getName();
        if (userId.equals(ownerId.toString())) {
            return true;
        } else {
            return userServiceClient.isUserInGroup(ownerId, UUID.fromString(userId));
        }
    }

    @Override
    public boolean hasPrimaryAuthority(UUID ownerId) {
        log.debug(SecurityContextHolder.getContext().getAuthentication().getName());
        UUID curUserid = UUID.fromString(SecurityContextHolder
            .getContext()
            .getAuthentication()
            .getName());
        return ownerId.equals(curUserid);
    }
}
