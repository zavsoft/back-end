package net.zavsoft.museum.keycloaksecurity;

import lombok.Getter;

@Getter
public enum Roles {

    MODERATOR(KeycloakConstants.ROLE_PREFIX + "MODERATOR"),
    ADMIN(KeycloakConstants.ROLE_PREFIX + "ADMIN"),
    MUSEUM(KeycloakConstants.ROLE_PREFIX + "MUSEUM");

    final String role;

    Roles(String name){
        this.role = name;
    }
}