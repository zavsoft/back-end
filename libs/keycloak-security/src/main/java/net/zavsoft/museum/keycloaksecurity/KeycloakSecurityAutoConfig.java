package net.zavsoft.museum.keycloaksecurity;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

//TODO Убрать permitAll
@EnableWebSecurity
@RequiredArgsConstructor
@Configuration
@EnableFeignClients
@EnableConfigurationProperties(value = {KeycloakProperties.class})
public class KeycloakSecurityAutoConfig {

    private final UserServiceClient userServiceClient;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, KeycloakProperties keycloakProperties) throws Exception {
        http.oauth2ResourceServer(oauth2 -> oauth2.jwt(jwtConfigurer -> jwtConfigurer.jwtAuthenticationConverter(
            jwtAuthenticationConverter(keycloakProperties))));
        return http.csrf()
                   .disable()
                   .authorizeHttpRequests(auth -> auth.requestMatchers("/users/**")
                                                      .permitAll()
                                                      .requestMatchers("/museum/register")
                                                      .permitAll()
                                                      .requestMatchers("/projects/get/**")
                                                      .permitAll()
                                                      .requestMatchers("/internal/**")
                                                      .permitAll()
                                                      .requestMatchers("/swagger-ui/**")
                                                      .permitAll()
                                                      .requestMatchers("/v3/api-docs/**")
                                                      .permitAll()
                                                      .anyRequest()
                                                      .authenticated())
                   .build();
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(KeycloakProperties keycloakProperties) {
        var converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new JwtAuthConverter(keycloakProperties));
        return converter;
    }

    @Bean
    public KeycloakUserDetailsUtils keycloakUserDetailsService() {
        return new KeycloakUserDetailsUtils();
    }

    @Bean
    public AuthorityService authorityService() {
        return new AuthorityServiceImpl(userServiceClient);
    }

}
