package net.zavsoft.museum.keycloaksecurity;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(name = "UserServiceClient", url = "${services.gateway}")
public interface UserServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/auth/internal/isUserInGroup")
    Boolean isUserInGroup(@RequestParam("groupId") UUID groupId, @RequestParam("userId") UUID userId);

}
