package net.zavsoft.museum.config;

import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@Slf4j
@AutoConfiguration
@EnableConfigurationProperties(MinioConfiguration.class)
@ConditionalOnProperty(prefix = "minio", name = "enabled", havingValue = "true")
public class MinioAutoConfig {

    @Bean
    public MinioFileStorageClient bodyStorageClient(MinioConfiguration minioConfiguration) {
        return new MinioFileStorageClient(minioClient(minioConfiguration), minioConfiguration);
    }

    @Bean
    public MinioClient minioClient(MinioConfiguration minioConfiguration) {
        return MinioClient.builder()
                          .credentials(minioConfiguration.getAccessKey(), minioConfiguration.getSecretKey())
                          .endpoint(minioConfiguration.getEndpoint())
                          .build();
    }

}