package net.zavsoft.museum.exception;

public class CheckFileExistException extends MinioOperationException {

    public CheckFileExistException(Throwable cause) {
        super("Ошибка при проверке существования файла в MinIO", cause);
    }

}