package net.zavsoft.museum.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;
import ru.zavsoft.common.utils.YamlPropertySourceFactory;

@Validated
@Data
@NoArgsConstructor
@Configuration
@PropertySource(value = "classpath:application-minio.yml", factory = YamlPropertySourceFactory.class)
@ConfigurationProperties(prefix = "minio")
public class MinioConfiguration {

    private boolean enabled;

    private String endpoint;

    private String accessKey;

    private String secretKey;

    private String minioBucketName;

}