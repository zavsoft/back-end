package net.zavsoft.museum.service.impl;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.StatObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.http.Method;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.config.MinioConfiguration;
import net.zavsoft.museum.exception.CheckFileExistException;
import net.zavsoft.museum.exception.DeleteFileException;
import net.zavsoft.museum.exception.ReceiveFileException;
import net.zavsoft.museum.exception.SaveFileException;
import net.zavsoft.museum.service.FileStorageClient;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@RequiredArgsConstructor
public class MinioFileStorageClient implements FileStorageClient {

    private final MinioClient minioClient;

    private final MinioConfiguration configuration;

    @Override
    public void save(byte[] bytes, String fileType, UUID id) {
        try (InputStream inputStream = new ByteArrayInputStream(bytes)) {
            createBucket();
            minioClient.putObject(PutObjectArgs.builder()
                                               .bucket(configuration.getMinioBucketName())
                                               .object("%s.%s".formatted(id, fileType))
                                               .stream(inputStream, bytes.length, -1)
                                               .build());
        } catch (Exception e) {
            log.warn("failed to save body to minio: {}", e.toString());
            throw new SaveFileException(e);
        }
    }

    @Override
    public InputStream find(UUID id, String fileType) {
        try {
            return minioClient.getObject(GetObjectArgs.builder()
                                                      .bucket(configuration.getMinioBucketName())
                                                      .object("%s.%s".formatted(id, fileType))
                                                      .build());
        } catch (Exception e) {
            throw new ReceiveFileException(e);
        }
    }

    @SneakyThrows
    private void createBucket() {
        var found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(configuration.getMinioBucketName()).build());

        if (!found) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(configuration.getMinioBucketName()).build());
        }
    }

    public void delete(String objectName) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                                                     .bucket(configuration.getMinioBucketName())
                                                     .object(objectName)
                                                     .build());
        } catch (Exception e) {
            throw new DeleteFileException(e);
        }
    }

    public String getTempUrl(String objectName, String type) {
        try {
            Map<String, String> reqParams = new HashMap<>();
            reqParams.put("response-content-type", "application/json");
            String url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                                                                                    .method(Method.GET)
                                                                                    .bucket(configuration.getMinioBucketName())
                                                                                    .object("%s.%s".formatted(objectName, type))
                                                                                    .expiry(2, TimeUnit.HOURS)
                                                                                    .extraQueryParams(reqParams)
                                                                                    .build());
            System.out.println(url);
            return url;
        } catch (Exception e) {
            log.error("Minio shit its pants: {}", e.getMessage(), e);
            return null;
        }
    }

    public boolean exist(String id) {
        try {
            minioClient.statObject(StatObjectArgs.builder().bucket(configuration.getMinioBucketName()).object(id).build());
            return true;
        } catch (ErrorResponseException e) {
            log.error("", e);
            return false;
        } catch (Exception e) {
            throw new CheckFileExistException(e);
        }
    }

}