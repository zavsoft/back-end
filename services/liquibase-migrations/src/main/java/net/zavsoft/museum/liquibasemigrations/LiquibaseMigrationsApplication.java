package net.zavsoft.museum.liquibasemigrations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquibaseMigrationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquibaseMigrationsApplication.class, args);
	}

}
