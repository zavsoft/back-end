package net.zavsoft.museum.album.exceptions;

public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException(String resourceType) {
        super(String.format("Resource of type %s not found", resourceType));
    }

}
