package net.zavsoft.museum.album.clients;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig {

    @Bean
    RequestInterceptor feignRequestInterceptor() {
        return RequestTemplate::request;
    }
}