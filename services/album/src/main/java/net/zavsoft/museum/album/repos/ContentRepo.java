package net.zavsoft.museum.album.repos;

import net.zavsoft.museum.album.entities.Album;
import net.zavsoft.museum.album.entities.Content;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContentRepo extends CrudRepository<Content, UUID> {}
