package net.zavsoft.museum.album.services;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.album.entities.FileMeta;
import net.zavsoft.museum.album.repos.FileMetaRepository;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileService {

    private final MinioFileStorageClient minioClient;
    private final FileMetaRepository fileMetaRepository;

    public FileMeta saveFile(Resource resource) throws IOException {

        final FileMeta fileMeta = new FileMeta(Objects.requireNonNull(resource.getFilename()));

        UUID minioId = UUID.randomUUID();
        minioClient.save(resource.getContentAsByteArray(), fileMeta.getExtension(), minioId);

        fileMeta.setMinioId(minioId);
        fileMetaRepository.save(fileMeta);
        return fileMeta;

    }

    public Resource getFile(FileMeta fileMeta) throws IOException {
        return new ByteArrayResource(minioClient.find(fileMeta.getMinioId(), fileMeta.getExtension()).readAllBytes());

    }

}
