package net.zavsoft.museum.album.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Entity
@Accessors(chain = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    protected UUID id;

    @Enumerated(value = EnumType.STRING)
    protected ContentType contentType;

    protected Content() {

    }

    public enum ContentType {
        IMAGE,
        VIDEO,
        TEXT
    }
}
