package net.zavsoft.museum.album.repos;

import net.zavsoft.museum.album.entities.Album;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AlbumRepo extends CrudRepository<Album, UUID> {

    List<Album> findAllByOwner(UUID owner);

    @Query(nativeQuery = true, value = "select * from album a left join album_exhibits ae on a.id = ae.album_id where ae.exhibits_id = :exhibitId")
    Optional<Album> findAlbumByExhibit(UUID exhibitId);


}
