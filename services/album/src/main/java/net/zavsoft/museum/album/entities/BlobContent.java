package net.zavsoft.museum.album.entities;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.experimental.Accessors;

@Entity
@Data
@Accessors(chain = true)
public class BlobContent extends Content {
    byte[] blob;
}
