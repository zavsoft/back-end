package net.zavsoft.museum.album.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import net.zavsoft.museum.album.dto.content.ContentDto;
import net.zavsoft.museum.album.entities.Content;

import java.util.UUID;

@Data
@Builder
@Accessors(chain = true)
public class ExhibitDto {

    private UUID id;
    private String name;
    private String dates;
    private String description;
    private Content.ContentType contentType;

}
