//package net.zavsoft.museum.album.dto.content;
//
//import lombok.Getter;
//import lombok.Setter;
//import lombok.experimental.Accessors;
//import net.zavsoft.museum.album.entities.Content;
//
//@Getter
//@Setter
//@Accessors(chain = true)
//public class VideContentDto extends ContentDto {
//
//    String videoUrl;
//
//    public VideContentDto() {
//        super(Content.ContentType.VIDEO);
//    }
//
//    private VideContentDto(String videoUrl) {
//        super(Content.ContentType.VIDEO);
//        this.videoUrl = videoUrl;
//    }
//
//    @Override
//    public byte[] getBody() {
//        return videoUrl.getBytes();
//    }
//
//    public static VideContentDtoBuilder builder() {
//        return new VideContentDtoBuilder();
//    }
//
//    public static class VideContentDtoBuilder {
//
//        private String videoUrl;
//
//        VideContentDtoBuilder() {
//        }
//
//        public VideContentDtoBuilder videoUrl(String videoUrl) {
//            this.videoUrl = videoUrl;
//            return this;
//        }
//
//        public VideContentDto build() {
//            return new VideContentDto(this.videoUrl);
//        }
//
//        public String toString() {
//            return "VideContentDto.VideContentDtoBuilder(videoUrl=" + this.videoUrl + ")";
//        }
//
//    }
//
//}
