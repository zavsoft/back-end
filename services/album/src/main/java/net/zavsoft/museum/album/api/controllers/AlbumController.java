package net.zavsoft.museum.album.api.controllers;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.album.dto.AlbumCreateDto;
import net.zavsoft.museum.album.dto.AlbumDto;
import net.zavsoft.museum.album.entities.Album;
import net.zavsoft.museum.album.services.AlbumService;
import net.zavsoft.museum.keycloaksecurity.AuthorityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController()
@RequestMapping("/albums")
@RequiredArgsConstructor
public class AlbumController {

    private static final Logger log = LoggerFactory.getLogger(AlbumController.class);

    private final AlbumService albumService;
    private final AuthorityService authorityService;

    @GetMapping("/getAllOwnersAlbum/{ownerId}")
    public List<AlbumDto> getAllOwnersAlbum(@PathVariable UUID ownerId) {
        return albumService.findAllOwnerAlbums(ownerId);
    }

    @GetMapping("/{albumId}")
    public Optional<AlbumDto> getAlbum(@PathVariable UUID albumId) {
        return albumService
                .findAlbumById(albumId)
                .map(albumService::mapToAlbumDto);
    }

    @GetMapping("/{albumId}/img")
    public ResponseEntity<Resource> getImage(@PathVariable UUID albumId) throws IOException {

        var image = albumService.getImage(albumId);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "image/jpeg");
        headers.add(HttpHeaders.CONTENT_LENGTH, String.valueOf(image.contentLength()));
        headers.add("Content-Disposition", "attachment");
        return new ResponseEntity<>(image, headers, HttpStatus.OK);

    }



    @PostMapping(value = "/{ownerId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AlbumDto> createAlbum(@PathVariable UUID ownerId, @RequestBody AlbumCreateDto albumDto) {
        if (authorityService.hasAuthority(albumDto.getOwnerId())) {
            var album = albumService.createAlbum(albumDto);
            return ResponseEntity.ok(albumService.mapToAlbumDto(album));
        }
        return ResponseEntity.status(403).build();
    }

    @PutMapping("/{ownerId}/{albumId}")
    public ResponseEntity<AlbumDto> updateAlbum(AlbumCreateDto album, UUID albumId) {
        var albumEntity = albumService.findAlbumById(albumId).orElse(null);
        if (albumEntity == null) {
            return ResponseEntity.status(404).build();
        }
        if (authorityService.hasAuthority(albumEntity.getOwner())) {
            albumService.updateAlbum(album, albumId);
        }
        return ResponseEntity.status(403).build();
    }

    @PutMapping(value = "/{albumId}/img", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<AlbumDto> updateAlbumImage(@RequestPart("file") MultipartFile file, @PathVariable UUID albumId) {
        var albumEntity = albumService.findAlbumById(albumId).orElse(null);
        if (albumEntity == null) {
            return ResponseEntity.status(404).build();
        }
        if (authorityService.hasAuthority(albumEntity.getOwner())) {
            try {
                albumService.updateImage(file.getResource(), albumId);
                return ResponseEntity.status(200).build();
            } catch (Exception e) {
                return ResponseEntity.status(400).build();
            }
        }
        return ResponseEntity.status(403).build();
    }

    @DeleteMapping("/{ownerId}/{albumId}")
    public void deleteAlbum(@PathVariable UUID albumId) {
        var albumEntity = albumService.findAlbumById(albumId).orElse(null);
        if (authorityService.hasAuthority(albumEntity.getOwner())) {
            albumService.deleteAlbum(albumId);
        }
    }
}
