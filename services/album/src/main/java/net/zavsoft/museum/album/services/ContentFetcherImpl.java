package net.zavsoft.museum.album.services;

import net.zavsoft.museum.album.dto.content.ContentDto;
import net.zavsoft.museum.album.entities.BlobContent;
import net.zavsoft.museum.album.entities.Content;
import net.zavsoft.museum.album.entities.MinioContent;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;

@Service
public class ContentFetcherImpl {

    private static final Logger log = LoggerFactory.getLogger(ContentFetcherImpl.class);

    MinioFileStorageClient minioFileStorageClient;

}
