package net.zavsoft.museum.album.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Entity
@Data
@Accessors(chain = true)
public class MinioContent extends Content {

    @OneToOne
    FileMeta fileMeta;

}
