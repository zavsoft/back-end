package net.zavsoft.museum.album.services;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.album.dto.AlbumCreateDto;
import net.zavsoft.museum.album.dto.AlbumDto;
import net.zavsoft.museum.album.entities.Album;
import net.zavsoft.museum.album.repos.AlbumRepo;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class AlbumService {

    private final AlbumRepo albumRepo;
    private final FileService fileService;

    public Album createAlbum(AlbumCreateDto albumCreateDto) {
        net.zavsoft.museum.album.entities.Album album = net.zavsoft.museum.album.entities.Album.builder()
                                                                                               .name(albumCreateDto.getName())
                                                                                               .description(albumCreateDto.getDescription())
                                                                                               .owner(albumCreateDto.getOwnerId())
                                                                                               .build();
        return albumRepo.save(album);

    }

    public Optional<Album> findAlbumById(UUID albumId) {
        return albumRepo.findById(albumId);
    }

    public Optional<Album> findAlbumByExhibitId(UUID exhibitId) {
        return albumRepo.findAlbumByExhibit(exhibitId);
    }



    public void updateAlbum(AlbumCreateDto album, UUID albumId) {

        albumRepo.findById(albumId).ifPresent(a -> {
            a.setName(album.getName()).setDescription(album.getDescription());
            albumRepo.save(a);
        });

    }

    public void updateImage(Resource resource, UUID albumId) throws IOException {

        Album album = albumRepo.findById(albumId).orElseThrow();
        var fileMeta = fileService.saveFile(resource);
        album.setImg(fileMeta);
        albumRepo.save(album);

    }

    public void deleteAlbum(UUID albumId) {
        albumRepo.findById(albumId).ifPresent(a -> {
            albumRepo.delete(a);
        });
    }

    public List<AlbumDto> findAllOwnerAlbums(UUID ownerId) {
        return albumRepo.findAllByOwner(ownerId)
                        .stream()
                        .map(this::mapToAlbumDto)
                        .toList();

    }

    public Resource getImage(UUID albumId) throws IOException {
        Album album = albumRepo.findById(albumId).orElseThrow();
        return fileService.getFile(album.getImg());
    }

    public AlbumDto mapToAlbumDto(Album album) {
        return new AlbumDto().setId(album.getId()).setName(album.getName()).setDescription(album.getDescription());
    }


}
