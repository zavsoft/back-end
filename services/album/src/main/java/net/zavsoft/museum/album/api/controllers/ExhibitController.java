package net.zavsoft.museum.album.api.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.album.api.ExhibitApi;
import net.zavsoft.museum.album.dto.ExhibitDto;
import net.zavsoft.museum.album.dto.content.ContentDto;
import net.zavsoft.museum.album.entities.Album;
import net.zavsoft.museum.album.entities.Content;
import net.zavsoft.museum.album.entities.Exhibit;
import net.zavsoft.museum.album.entities.FileMeta;
import net.zavsoft.museum.album.exceptions.ResourceNotFoundException;
import net.zavsoft.museum.album.services.AlbumService;
import net.zavsoft.museum.album.services.ContentFetcherImpl;
import net.zavsoft.museum.album.services.ExhibitService;
import net.zavsoft.museum.keycloaksecurity.AuthorityService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@RestController()
@RequestMapping("/exhibits")
@RequiredArgsConstructor
public class ExhibitController {

    private final AlbumService albumService;

    private final ExhibitService exhibitService;

    private final AuthorityService authorityService;

    private final ContentFetcherImpl contentFetcher;


    @GetMapping("/getExhibitsFromAlbum/{albumId}")
    public ResponseEntity<List<ExhibitDto>> getExhibits(@PathVariable UUID albumId) {
        var album = albumService.findAlbumById(albumId).orElseThrow();

        if (authorityService.hasAuthority(album.getOwner())) {

            List<ExhibitDto> exhibitDtos = album.getExhibits()
                                                .stream()
                                                .map(exhibit -> ExhibitDto.builder()
                                                                          .name(exhibit.getName())
                                                                          .description(exhibit.getDescription())
                                                                          .contentType(exhibit.getContent().getContentType())
                                                                          .build())
                                                .toList();

            return ResponseEntity.ok(exhibitDtos);
        }

        return ResponseEntity.status(403).build();
    }

    @GetMapping("/{exhibitId}")
    public ResponseEntity<ExhibitDto> getExhibit(@PathVariable UUID exhibitId) {
        Album album = albumService.findAlbumByExhibitId(exhibitId).orElseThrow();
        if (authorityService.hasAuthority(album.getOwner())) {

            return ResponseEntity.of(exhibitService.getExhibit(exhibitId)
                                                   .map(e -> ExhibitDto.builder()
                                                                       .id(e.getId())
                                                                       .dates(e.getDates())
                                                                       .contentType(e.getContent().getContentType())
                                                                       .build()));
        }

        return ResponseEntity.status(403).build();
    }

    @GetMapping("/{exhibitId}/body")
    public ResponseEntity<Resource> getExhibitContent(@PathVariable UUID exhibitId)
        throws IOException, ResourceNotFoundException {
        Album album = albumService.findAlbumByExhibitId(exhibitId).orElseThrow();
        if (authorityService.hasAuthority(album.getOwner())) {
            Resource body = exhibitService.getExhibitBody(exhibitId);
            FileMeta fileMeta = exhibitService.getFileMeta(exhibitId);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION,
                        String.format("attachment; filename=%s.%s", fileMeta.getOriginalName(), fileMeta.getExtension()));
            return ResponseEntity.ok()
                                 .headers(headers)
                                 .contentLength(body.contentLength())
                                 .contentType(MediaType.parseMediaType("application/octet-stream"))
                                 .body(body);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/{albumId}")
    public ResponseEntity<ExhibitDto> createExhibit(@PathVariable UUID albumId,
                                                    @RequestBody ExhibitDto exhibit
    ) throws IOException {
        var album = albumService.findAlbumById(albumId).orElseThrow(() -> new RuntimeException("Album not found"));
        if (authorityService.hasAuthority(album.getOwner())) {
            Exhibit exhibitEntity = exhibitService.createExhibit(exhibit, albumId);
            return ResponseEntity.ok(ExhibitDto.builder()
                                               .id(exhibitEntity.getId())
                                               .name(exhibitEntity.getName())
                                               .description(exhibitEntity.getDescription())
                                               .dates(exhibitEntity.getDates())
                                               .build());
        }
        return ResponseEntity.status(403).build();
    }

    @PutMapping( value = "/{exhibitId}/updateContent", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ExhibitDto> setContent(@PathVariable UUID exhibitId,
                                                 @RequestParam("contentType") Content.ContentType contentType,
                                                 @RequestPart("file") MultipartFile file
    ) throws IOException {
        Album album = albumService.findAlbumByExhibitId(exhibitId).orElseThrow();
        if (authorityService.hasAuthority(album.getOwner())) {

            exhibitService.updateExhibitContent(exhibitId, contentType, file);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(403).build();
    }

    @PutMapping("/{exhibitId}")
    public ResponseEntity<ExhibitDto> updateExhibit(@PathVariable UUID exhibitId, @RequestBody ExhibitDto exhibit) {
        Album album = albumService.findAlbumByExhibitId(exhibitId).orElseThrow();
        if (authorityService.hasAuthority(album.getOwner())) {
            Exhibit exhibitEntity = exhibitService.updateExhibit(exhibitId, exhibit);
            return ResponseEntity.ok(ExhibitDto.builder()
                                               .name(exhibitEntity.getName())
                                               .description(exhibitEntity.getDescription())
                                               .dates(exhibitEntity.getDates())
                                               .contentType(exhibitEntity.getContent().getContentType())
                                               .build());
        }
        return ResponseEntity.status(403).build();
    }

    @DeleteMapping("/{exhibitId}")
    public ResponseEntity<Object> deleteExhibit(@PathVariable UUID exhibitId) {
        Album album = albumService.findAlbumByExhibitId(exhibitId).orElseThrow();
        if (authorityService.hasAuthority(album.getOwner())) {
            exhibitService.deleteExhibit(exhibitId);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(403).build();
    }

}
