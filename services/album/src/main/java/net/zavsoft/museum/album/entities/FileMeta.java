package net.zavsoft.museum.album.entities;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class FileMeta implements Serializable {
    @Id
    UUID MinioId;

    String originalName;
    String extension;

    public FileMeta(String originalName) {
        String[] split = originalName.split("\\.");
        if (split.length >= 2) {
            this.originalName = Arrays.stream(split).limit(split.length - 1).collect(Collectors.joining("."));
            this.extension = split[split.length - 1];
        } else {
            throw new IllegalArgumentException("Invalid file name: " + originalName);
        }
    }

//    public static void main(String[] args) {
//        final FileMeta fileMeta = new FileMeta("file.1.jpg");
//        System.out.println(fileMeta.originalName);
//        System.out.println(fileMeta.extension);
//    }
}
