package net.zavsoft.museum.album.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AlbumCreateDto {

    private UUID ownerId;
    private String name;
    private String description;

}
