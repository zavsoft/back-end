package net.zavsoft.museum.album.api;

import net.zavsoft.museum.album.dto.AlbumCreateDto;
import net.zavsoft.museum.album.dto.AlbumDto;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface AlbumApi {

    List<AlbumDto> getAllOwnersAlbum(UUID ownerId);

    ResponseEntity<AlbumDto> createAlbum(UUID ownerId, AlbumCreateDto album);

    ResponseEntity<AlbumDto> updateAlbum(AlbumCreateDto album, UUID albumId);

    ResponseEntity<AlbumDto> updateAlbumImage(Resource resource, UUID albumId);

    void deleteAlbum(UUID albumId);

}
