//package net.zavsoft.museum.album.dto.content;
//
//import lombok.Getter;
//import lombok.Setter;
//import lombok.experimental.Accessors;
//import net.zavsoft.museum.album.entities.Content;
//
//
//@Getter
//@Setter
//@Accessors(chain = true)
//public class TextContentDto extends ContentDto {
//
//    String text;
//
//    public TextContentDto() {
//        super(Content.ContentType.TEXT);
//    }
//
//    private TextContentDto(String text) {
//        super(Content.ContentType.TEXT);
//        this.text = text;
//    }
//
//    @Override
//    public byte[] getBody() {
//        return text.getBytes();
//    }
//
//    public static TextContentDtoBuilder builder() {
//        return new TextContentDtoBuilder();
//    }
//
//    public static class TextContentDtoBuilder {
//
//        private String text;
//
//        TextContentDtoBuilder() {
//        }
//
//        public TextContentDtoBuilder text(String text) {
//            this.text = text;
//            return this;
//        }
//
//        public TextContentDto build() {
//            return new TextContentDto(this.text);
//        }
//
//        public String toString() {
//            return "TextContentDto.TextContentDtoBuilder(text=" + this.text + ")";
//        }
//
//    }
//
//}
