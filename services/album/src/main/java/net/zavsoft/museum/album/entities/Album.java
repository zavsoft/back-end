package net.zavsoft.museum.album.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private String name;
    private String description;

    private UUID owner;

    @OneToOne
    private FileMeta img;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Exhibit> exhibits;
}
