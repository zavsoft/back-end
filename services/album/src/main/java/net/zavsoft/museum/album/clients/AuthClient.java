package net.zavsoft.museum.album.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "authClient", url="${services.gateway}"+"/auth")
public interface AuthClient {

    @PostMapping("/validate")
    ResponseEntity<String> validate(@RequestParam String token);
    @PostMapping("/validate1")
    ResponseEntity<String> isUser(@RequestParam String token);
}
