package net.zavsoft.museum.album.repos;

import net.zavsoft.museum.album.entities.Exhibit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExhibitRepo extends CrudRepository<Exhibit, UUID> {
}
