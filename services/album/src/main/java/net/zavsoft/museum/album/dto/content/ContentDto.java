package net.zavsoft.museum.album.dto.content;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import net.zavsoft.museum.album.entities.Content;
import net.zavsoft.museum.album.entities.FileMeta;
import org.springframework.core.io.Resource;

//@JsonTypeInfo(
//    use = JsonTypeInfo.Id.NAME,
//    include = JsonTypeInfo.As.PROPERTY,
//    property = "contentType")
//@JsonSubTypes({
//    @JsonSubTypes.Type(value = VideContentDto.class, name = "VIDEO"),
//    @JsonSubTypes.Type(value = TextContentDto.class, name = "TEXT"),
//    @JsonSubTypes.Type(value = ImageContentDto.class, name = "IMAGE"),
//})
@Accessors(chain = true)
@Getter
@Setter
public class ContentDto {

    public FileMeta fileMeta;
    public Content.ContentType contentType;

    public ContentDto() {
    }

}
