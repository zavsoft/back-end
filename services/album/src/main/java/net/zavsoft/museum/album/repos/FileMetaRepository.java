package net.zavsoft.museum.album.repos;

import net.zavsoft.museum.album.entities.FileMeta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileMetaRepository extends JpaRepository<FileMeta, UUID> {
}
