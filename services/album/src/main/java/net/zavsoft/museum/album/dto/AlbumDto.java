package net.zavsoft.museum.album.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true, fluent = false)
public class AlbumDto {

    private UUID id;
    private String name;
    private String description;

}
