package net.zavsoft.museum.album.services;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.album.dto.ExhibitDto;
import net.zavsoft.museum.album.dto.content.ContentDto;
import net.zavsoft.museum.album.entities.*;
import net.zavsoft.museum.album.exceptions.ResourceNotFoundException;
import net.zavsoft.museum.album.repos.AlbumRepo;
import net.zavsoft.museum.album.repos.ContentRepo;
import net.zavsoft.museum.album.repos.ExhibitRepo;
import org.springframework.core.io.Resource;
import org.springframework.security.util.InMemoryResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ExhibitService {

    private final AlbumRepo albumRepo;
    private final ExhibitRepo exhibitRepo;
    private final ContentRepo contentRepo;
    private final FileService fileService;

    public List<Exhibit> findAllExhibitsInAlbum(UUID albumId) {
        return albumRepo.findById(albumId)
                           .map(Album::getExhibits)
                           .orElseThrow(() -> new RuntimeException("Album not found"));
    }

    public Exhibit createExhibit(ExhibitDto exhibit, UUID albumId) throws IOException {
        Exhibit exhibitEntity = new Exhibit().setName(exhibit.getName())
                .setDates(exhibit.getDates())
                .setDescription(exhibit.getDescription());


//        var fileMeta = fileService.saveFile(file.getResource());
//        var minioContent = new MinioContent()
//                .setFileMeta(fileMeta)
//                .setContentType(exhibit.getContentType());
//
//        contentRepo.save(minioContent);
//
//        exhibitEntity.setContent(minioContent);
        exhibitRepo.save(exhibitEntity);

        albumRepo.findById(albumId).ifPresent(album -> {
            album.getExhibits().add(exhibitEntity);
            albumRepo.save(album);
        });

        return exhibitEntity;

    }

    public Optional<Exhibit> getExhibit(UUID exhibitId) {
        return exhibitRepo.findById(exhibitId);
    }

    public void updateExhibitContent(UUID exhibitId, Content.ContentType contentType, MultipartFile file) throws IOException {
        Exhibit exhibitEntity = exhibitRepo.findById(exhibitId)
                                           .orElseThrow(() -> new RuntimeException("Exhibit not found"));

        var fileMeta = fileService.saveFile(file.getResource());
        Content content = new MinioContent()
                .setFileMeta(fileMeta)
                .setContentType(contentType);

        contentRepo.save(content);
        Content oldContent = exhibitEntity.getContent();
        exhibitEntity.setContent(content);

        exhibitRepo.save(exhibitEntity);
        if (oldContent != null) {
            contentRepo.delete(oldContent);
        }
    }

    public Exhibit updateExhibit(UUID exhibitId, ExhibitDto exhibit) {
        Exhibit exhibitEntity = exhibitRepo.findById(exhibitId)
                                           .orElseThrow(() -> new RuntimeException("Exhibit not found"));

        if (Objects.nonNull(exhibit.getName())) {
            exhibitEntity.setName(exhibit.getName());
        }
        if (Objects.nonNull(exhibit.getDescription())) {
            exhibitEntity.setDescription(exhibit.getDescription());
        }
        if (Objects.nonNull(exhibit.getDates())) {
            exhibitEntity.setDates(exhibit.getDates());
        }
        exhibitRepo.save(exhibitEntity);

        return exhibitEntity;
    }

    public Resource getExhibitBody(UUID exhibitId) throws IOException {
        Exhibit exhibit = exhibitRepo.findById(exhibitId).orElseThrow();
        if (exhibit.getContent() instanceof BlobContent blobContent) {
            return new InMemoryResource(blobContent.getBlob());
        }
        if (exhibit.getContent() instanceof MinioContent minioContent) {
            return fileService.getFile(minioContent.getFileMeta());
        }
        throw new RuntimeException("Exhibit not blob");
    }


    public FileMeta getFileMeta(UUID exhibitId) throws ResourceNotFoundException {
        Exhibit exhibit = exhibitRepo.findById(exhibitId).orElseThrow(() -> new ResourceNotFoundException("Exhibit"));
        return ((MinioContent) exhibit.getContent()).getFileMeta();
    }

    public void deleteExhibit(UUID exhibitId) {
        exhibitRepo.deleteById(exhibitId);
    }

}
