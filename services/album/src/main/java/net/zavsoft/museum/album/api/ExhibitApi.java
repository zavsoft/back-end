package net.zavsoft.museum.album.api;

import net.zavsoft.museum.album.dto.ExhibitDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface ExhibitApi {

    ResponseEntity<List<ExhibitDto>> getExhibits(UUID albumId);

    ResponseEntity<ExhibitDto> createExhibit(UUID albumId, ExhibitDto exhibit);

    ResponseEntity<ExhibitDto> updateExhibit(UUID exhibitId, ExhibitDto exhibit);

    ResponseEntity<Object> deleteExhibit(UUID exhibitId);

}
