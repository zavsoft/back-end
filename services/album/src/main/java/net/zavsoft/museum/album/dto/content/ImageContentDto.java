//package net.zavsoft.museum.album.dto.content;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.experimental.Accessors;
//import net.zavsoft.museum.album.entities.Content;
//
//@Getter
//@Setter
//@Accessors(chain = true)
//public class ImageContentDto extends ContentDto {
//
//    @JsonIgnore
//    byte[] image;
//
//    public ImageContentDto() {
//        super(Content.ContentType.IMAGE);
//    }
//
//    private ImageContentDto(byte[] image) {
//        super(Content.ContentType.IMAGE);
//        this.image = image;
//    }
//
//    @Override
//    @JsonIgnore
//    public byte[] getBody() {
//        return image;
//    }
//
//    public static ImageContentDtoBuilder builder() {
//        return new ImageContentDtoBuilder();
//    }
//
//    public static class ImageContentDtoBuilder {
//
//        private byte[] image;
//
//        ImageContentDtoBuilder() {
//        }
//
//        public ImageContentDtoBuilder image(byte[] image) {
//            this.image = image;
//            return this;
//        }
//
//        public ImageContentDto build() {
//            return new ImageContentDto(this.image);
//        }
//
//        public String toString() {
//            return "ImageContentDto.ImageContentDtoBuilder(image=" + java.util.Arrays.toString(this.image) + ")";
//        }
//
//    }
//
//}
