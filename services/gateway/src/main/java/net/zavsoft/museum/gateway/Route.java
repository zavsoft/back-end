package net.zavsoft.museum.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Route {
    @Value("${services.auth}")
    String authUrl;
    @Value("${services.album}")
    String album;
    @Value("${services.company}")
    String company;
    @Value("${services.project}")
    String project;

    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                    .path("/auth/**")
                    .filters(f -> f.rewritePath("^/auth", ""))
                    .uri(authUrl))
                .route(p -> p
                    .path("/albums/**")
                    .filters(f -> f.rewritePath("^/albums", ""))
                    .uri(album))
                .route(p -> p
                    .path("/project/**")
                    .filters(f -> f.rewritePath("^/project", ""))
                    .uri(project))
                .route(p -> p
                    .path("/exhibition/**")
                    .filters(f -> f.rewritePath("^/exhibition", ""))
                    .uri(project))
                .build();
    }
}
