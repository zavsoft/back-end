package net.zavsoft.museum.userservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;
import java.util.UUID;

@FeignClient(name = "ProjectsClient", url = "${services.projects}" + "/projects")
public interface ProjectClient {

    @GetMapping("/{projectId}/owner")
    Optional<UUID> getOwnerId(@PathVariable UUID projectId);

}
