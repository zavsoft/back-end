package net.zavsoft.museum.userservice.controllers;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.userservice.controllers.api.GroupsApi;
import net.zavsoft.museum.userservice.services.GroupsService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class GroupsController implements GroupsApi {

    private final GroupsService service;

    @Override
    public void addGroup(UUID objectId) {
        service.addGroup(objectId);
    }

    @Override
    public void addGroup(UUID objectId, Set<UUID> userIds) {
        service.addGroup(objectId,userIds);
    }

    @Override
    public void deleteGroupById(UUID objectId) {
        service.deleteGroupById(objectId);
    }

    @Override
    public void addUser(UUID objectId, String email) {
        service.addUser(objectId, email);
    }

    @Override
    public void deleteUser(UUID objectId, UUID userId) {
        service.deleteUser(objectId, userId);
    }

    @Override
    public List<String> getUsersGroup() {
        return service.getUsersGroups();
    }

    @Override
    public List<String> getGroupMembers(UUID objectId) {
        return service.getUsersIdsFromGroup(objectId);
    }

}
