package net.zavsoft.museum.userservice.repos;

import net.zavsoft.museum.userservice.entities.Museum;

public interface MuseumRepository extends CommonUserRepository<Museum> {}
