package net.zavsoft.museum.userservice.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.zavsoft.museum.userservice.dtos.user.FullSimpleUserDto;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
@NoArgsConstructor
public class SimpleUser implements User {

    @Id
    private UUID id;

    private String avatarType;

    private String country;

    private String city;

    private String description;

    public SimpleUser(UUID id) {
        this.id = id;
    }

    public void update(FullSimpleUserDto userDto) {
        this.country = userDto.getCountry();
        this.city = userDto.getCity();
        this.description = userDto.getDescription();
    }

    @Override
    public FullSimpleUserDto toDto() {
        return new FullSimpleUserDto(id, avatarType, country, city, description);
    }

}
