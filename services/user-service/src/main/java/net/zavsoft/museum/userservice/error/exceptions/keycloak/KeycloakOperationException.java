package net.zavsoft.museum.userservice.error.exceptions.keycloak;

import jakarta.ws.rs.core.Response;
import lombok.Getter;

@Getter
public class KeycloakOperationException extends RuntimeException {

    protected final Response.StatusType statusInfo;

    public KeycloakOperationException(Response.StatusType statusInfo, String message) {
        super(message);
        this.statusInfo = statusInfo;
    }
}
