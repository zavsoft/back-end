package net.zavsoft.museum.userservice.dtos.museum;

import lombok.Getter;
import net.zavsoft.museum.userservice.dtos.FullUserInfoDto;
import org.keycloak.representations.idm.UserRepresentation;

@Getter
public class FullMuseumInfoDto extends FullUserInfoDto<FullMuseumDto> {

    public FullMuseumInfoDto(FullMuseumDto user, UserRepresentation userRepresentation, String avatarUrl) {
        super(user, userRepresentation, avatarUrl);
    }

}
