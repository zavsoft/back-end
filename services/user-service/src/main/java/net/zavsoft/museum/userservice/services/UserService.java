package net.zavsoft.museum.userservice.services;

import jakarta.validation.Valid;
import net.zavsoft.museum.keycloaksecurity.Roles;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface UserService {

    FullInfoDto getCurrentUser();

    UUID addUser(@Valid UserRegistrationDto userReg, List<Roles> roles);

    void setAvatar(UUID id, MultipartFile avatar) throws IOException;

    String getAvatar(@PathVariable UUID userId) throws IOException;

    //TODO отправлять сообщение на старую почту для проверки
    void changeEmail(UUID id, String newEmail);

    void deleteUser(UUID userId);

}
