package net.zavsoft.museum.userservice.repos;

import net.zavsoft.museum.userservice.entities.SimpleUser;

public interface UserRepository extends CommonUserRepository<SimpleUser> {}
