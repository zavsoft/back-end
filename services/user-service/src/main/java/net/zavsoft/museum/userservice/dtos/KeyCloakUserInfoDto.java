package net.zavsoft.museum.userservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class KeyCloakUserInfoDto {

    private final String username;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final boolean emailVerified;
}
