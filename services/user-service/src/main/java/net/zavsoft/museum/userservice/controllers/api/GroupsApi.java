package net.zavsoft.museum.userservice.controllers.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RequestMapping("/groups")
public interface GroupsApi {

    @GetMapping("/")
    List<String> getUsersGroup();

    @PostMapping("/create/{objectId}")
    void addGroup(@PathVariable UUID objectId);

    @PostMapping("/create/{objectId}/with-users")
    void addGroup(@PathVariable UUID objectId, @RequestBody Set<UUID> userIds);

    @DeleteMapping("/delete/{objectId}")
    void deleteGroupById(@PathVariable UUID objectId);

    @PutMapping("/{objectId}/add/{email}")
    void addUser(@PathVariable UUID objectId, @PathVariable String email);

    @PutMapping("/{objectId}/delete/{userId}")
    void deleteUser(@PathVariable UUID objectId, @PathVariable UUID userId);

    @GetMapping("/{objectId}/members")
    List<String> getGroupMembers(@PathVariable UUID objectId);
}
