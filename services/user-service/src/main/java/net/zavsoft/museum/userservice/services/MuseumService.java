package net.zavsoft.museum.userservice.services;

import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import net.zavsoft.museum.userservice.dtos.museum.FullMuseumInfoDto;

public interface MuseumService extends UserService{

    List<FullMuseumInfoDto> findByUsername(String username);

    void updateUser(@Valid FullMuseumInfoDto userDto);

    FullMuseumInfoDto findById(UUID id);
}
