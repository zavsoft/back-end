package net.zavsoft.museum.userservice.entities;

import net.zavsoft.museum.userservice.dtos.UserDto;

import java.util.UUID;

public interface User {

    UserDto toDto();

    UUID getId();

    String getAvatarType();

    void setAvatarType(String avatarType);

}
