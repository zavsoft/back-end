package net.zavsoft.museum.userservice.dtos.user;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import org.springframework.lang.Nullable;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class FullSimpleUserDto implements SimpleUserDto, FullInfoDto {

    @NotNull
    private final UUID id;

    @Nullable
    private final String avatarType;

    @Nullable
    private final String country;

    @Nullable
    private final String city;

    @Nullable
    private final String description;

}
