package net.zavsoft.museum.userservice.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MuseumAdmin {

    @Column(name = "adminName")
    private String museumName;
    @Column(name = "adminPosition")
    private String position;
    @Column(name = "adminPhone")
    private String phone;

}
