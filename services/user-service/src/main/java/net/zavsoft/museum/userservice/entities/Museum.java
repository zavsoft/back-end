package net.zavsoft.museum.userservice.entities;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.zavsoft.museum.userservice.dtos.museum.FullMuseumDto;
import org.springframework.lang.Nullable;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "museums")
@NoArgsConstructor
@AllArgsConstructor
public class Museum implements User {

    @Id
    private UUID id;

    private String avatarType;

    private UUID docId;

    private String museumName;

    private String country;

    private String city;

    private String address;

    private String description;

    private String organizationSite;

    private String directorName;

    private String phone;

    @Embedded
    @Nullable
    private MuseumAdmin museumAdmin;

    private boolean verified;

    public Museum(UUID id) {
        this.id = id;
    }

    @Override
    public FullMuseumDto toDto() {
        return new FullMuseumDto(id,
                                 avatarType,
                                 docId,
                                 museumName,
                                 country,
                                 city,
                                 address,
                                 description,
                                 organizationSite,
                                 directorName,
                                 phone,
                                 museumAdmin,
                                 verified);
    }

    public void update(FullMuseumDto dto) {
        this.country = dto.getCountry();
        this.city = dto.getCity();
        this.address = dto.getAddress();
        this.museumName = dto.getMuseumName();
        this.description = dto.getDescription();
        this.organizationSite = dto.getSite();
        this.phone = dto.getPhone();
        this.museumAdmin = dto.getMuseumAdmin();
    }

}
