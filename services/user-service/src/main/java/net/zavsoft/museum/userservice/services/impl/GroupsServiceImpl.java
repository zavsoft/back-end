package net.zavsoft.museum.userservice.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.keycloaksecurity.KeycloakUserDetailsUtils;
import net.zavsoft.museum.keycloaksecurity.Roles;
import net.zavsoft.museum.userservice.clients.ProjectClient;
import net.zavsoft.museum.userservice.services.GroupsService;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.AbstractUserRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class GroupsServiceImpl implements GroupsService {

    private final GroupsResource groupsResource;

    private final UsersResource userResource;

    @Override
    public void addGroup(UUID objectId) {
        //get upper level group
        var authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if (authorities.stream().anyMatch(x -> x.getAuthority().equals(Roles.MUSEUM.getRole()))) {
            GroupRepresentation subgroup = new GroupRepresentation();
            subgroup.setName(objectId.toString());
            Optional<GroupResource> group = groupsResource.groups()
                                                          .stream()
                                                          .filter(x -> KeycloakUserDetailsUtils.getUserId()
                                                                                               .toString()
                                                                                               .equals(x.getName()))
                                                          .findFirst()
                                                          .map(g -> groupsResource.group(g.getId()));
            if (group.isEmpty()) {
                throw new RuntimeException("GG");
            }
            group.get().subGroup(subgroup);
        } else {
            addGroup(objectId, Set.of());
        }
    }

    @Override
    public List<String> getUsersGroups() {
        var user = userResource.get(KeycloakUserDetailsUtils.getUserId().toString());
        return user.groups().stream().map(GroupRepresentation::getName).toList();
    }

    @Override
    public void deleteGroupById(UUID groupId) {
        groupsResource.groups().removeIf(x -> Objects.equals(x.getId(), groupId.toString()));
    }

    @Override
    public void addGroup(UUID objectId, Set<UUID> userIds) {
        GroupRepresentation group = new GroupRepresentation();
        group.setName(objectId.toString());
        try (var resp = groupsResource.add(group)) {
            if (resp.getStatus() != HttpStatus.CREATED.value()) {
                log.error("{}", resp.readEntity(String.class));
            }
            group.setId(CreatedResponseUtil.getCreatedId(resp));
            addUserToGroup(UUID.fromString(SecurityContextHolder.getContext().getAuthentication().getName()), group);
            userIds.forEach(x -> addUserToGroup(x, group));
        }
    }

    @Override
    public void deleteUser(UUID groupId, UUID userId) {
        UserResource user = userResource.get(userId.toString());
        if (user == null) {
            throw new IllegalArgumentException("Не найден пользователь с id: %s".formatted(userId));
        }
        GroupRepresentation group = groupsResource.groups(groupId.toString(), 0, 1)
                                                  .stream()
                                                  .filter(g -> g.getName().equals(groupId.toString()))
                                                  .findFirst()
                                                  .orElseThrow(() -> new IllegalArgumentException("Не найдена группа с id: %s".formatted(
                                                      groupId)));
        user.leaveGroup(group.getName());
    }

    @Override
    public void addUser(UUID groupId, String email) {
        Optional<GroupResource> group = groupsResource.groups()
                                                      .stream()
                                                      .filter(x -> groupId.toString().equals(x.getName()))
                                                      .findFirst()
                                                      .map(g -> groupsResource.group(g.getId()));
        if (group.isEmpty()) {
            throw new IllegalArgumentException("Не найдена группа с id: %s".formatted(groupId));
        }
        addUserToGroup(email, group.get().toRepresentation());
    }

    @Override
    public List<String> getUsersIdsFromGroup(UUID groupId) {
        Optional<GroupResource> group = groupsResource.groups()
                                                      .stream()
                                                      .filter(x -> groupId.toString().equals(x.getName()))
                                                      .findFirst()
                                                      .map(g -> groupsResource.group(g.getId()));
        if (group.isEmpty()) {
            throw new IllegalArgumentException("Не найдена группа с id: %s".formatted(groupId));
        }
        return group.get().members().stream().map(AbstractUserRepresentation::getId).toList();
    }

    public Boolean isUserInGroup(UUID groupId, UUID userId) {
        UserResource user = userResource.get(userId.toString());
        if (user == null) {
            throw new IllegalArgumentException("Не найден пользователь с id: %s".formatted(userId));
        }
        return user.groups().stream().map(GroupRepresentation::getName).anyMatch(gid -> Objects.equals(gid, groupId.toString()));
    }

    protected void addUserToGroup(UUID userId, GroupRepresentation group) {
        UserResource user = userResource.get(userId.toString());
        if (user == null) {
            throw new IllegalArgumentException("Не найден пользователь с id: %s".formatted(userId));
        }
        user.joinGroup(group.getId());
    }

    protected void addUserToGroup(String email, GroupRepresentation group) {
        UserResource user = userResource.searchByEmail(email, true)
                                        .stream()
                                        .findFirst()
                                        .map(x -> userResource.get(x.getId()))
                                        .orElseThrow(() -> new IllegalArgumentException("Не найден пользователь с email: %s".formatted(
                                            email)));
        user.joinGroup(group.getId());
    }

}

