package net.zavsoft.museum.userservice.services;

import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.keycloaksecurity.Roles;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import net.zavsoft.museum.userservice.dtos.KeyCloakUserInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import net.zavsoft.museum.userservice.entities.User;
import net.zavsoft.museum.userservice.error.exceptions.keycloak.KeycloakUserCreationException;
import net.zavsoft.museum.userservice.error.exceptions.keycloak.KeycloakUserDeleteException;
import net.zavsoft.museum.userservice.repos.CommonUserRepository;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.AbstractUserRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
public abstract class CommonUserService<T extends User> implements UserService {

    protected CommonUserRepository<T> repository;

    protected final UsersResource usersResource;

    protected final Keycloak keycloak;

    protected final RealmResource realmResource;

    protected final MinioFileStorageClient minio;

    protected abstract void saveUser(UUID userId);

    @Transactional
    public UUID addUser(@Valid UserRegistrationDto userReg, List<Roles> roles) {
        var res = new UserRegistrationDto(userReg.getUsername(), null, userReg.getFirstName(), userReg.getLastName());
        if (userReg.getPassword() == null) {
            res.setPassword(UUID.randomUUID().toString());
            userReg.setPassword(res.getPassword());
        }

        UserRepresentation user = createUser(userReg);
        UUID id;

        try (var resp = usersResource.create(user)) {
            if (resp.getStatus() != HttpStatus.CREATED.value()) {
                var exception = new KeycloakUserCreationException(resp.getStatusInfo());
                log.error("{} {}", exception.getMessage(), exception.getStatusInfo());
                throw exception;
            }
            id = UUID.fromString(CreatedResponseUtil.getCreatedId(resp));
            saveUser(id);
            roles.forEach(role -> assignRoleToUser(id.toString(), role));
        }

        return id;

    }

    public void deleteUser(UUID userId) {
        try (var resp = usersResource.delete(userId.toString())) {
            if (resp.getStatus() != HttpStatus.OK.value()) {
                var exception = new KeycloakUserDeleteException(resp.getStatusInfo());
                log.error("{} {}", exception.getMessage(), resp.getStatusInfo());
                throw exception;
            }
            repository.deleteById(userId);
        }
    }

    @Override
    public void setAvatar(UUID id, MultipartFile avatar) throws IOException {
        var user = findUserById(id);
        String[] name = avatar.getOriginalFilename().split("\\.");
        if (name.length < 2) {
            throw new RuntimeException("Некорректное имя файла");
        }
        user.setAvatarType(name[name.length - 1]);
        minio.save(avatar.getBytes(), user.getAvatarType(), id);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    public String getAvatar(@PathVariable UUID userId) {
        var user = findUserById(userId);
        return minio.getTempUrl(userId.toString(), user.getAvatarType());
    }

    @Override
    public void changeEmail(UUID id, String newEmail) {
    }

    protected T findUserById(UUID id) {
        return repository.findById(id).orElseThrow(() -> new NotFoundException("Пользователь с id = %s не найден".formatted(id)));
    }

    protected UserRepresentation createUser(UserRegistrationDto userReq) {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(userReq.getUsername());//login
        user.setEnabled(true);
        user.setFirstName(userReq.getFirstName());
        user.setLastName(userReq.getLastName());

        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setTemporary(false);
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(userReq.getPassword());
        user.setCredentials(List.of(credential));

        return user;
    }

    protected void updateUserRepresentation(UUID userId, KeyCloakUserInfoDto dto) {
        var userKeycloak = usersResource.get(userId.toString());
        var userRepresentation = userKeycloak.toRepresentation();
        userRepresentation.setFirstName(dto.getFirstName());
        userRepresentation.setLastName(dto.getLastName());
        userRepresentation.setEmail(dto.getEmail());
        userRepresentation.setEmailVerified(dto.isEmailVerified());
        userKeycloak.update(userRepresentation);
    }

    protected Iterable<T> findUserByUsername(String username) {
        var users = usersResource.search(username);
        return repository.findAllById(users.stream().map(AbstractUserRepresentation::getId).map(UUID::fromString).toList());
    }

    protected void assignRoleToUser(String userId, Roles roleName) {
        UserResource userResource = usersResource.get(userId);
        RoleRepresentation roleRepresentation = realmResource.roles().get(roleName.getRole()).toRepresentation();
        userResource.roles().realmLevel().add(List.of(roleRepresentation));
    }

}
