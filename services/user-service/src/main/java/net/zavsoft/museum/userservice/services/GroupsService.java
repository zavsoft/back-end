package net.zavsoft.museum.userservice.services;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface GroupsService {

    List<String> getUsersGroups();

    void addGroup(UUID objectId);

    void addGroup(UUID objectId, Set<UUID> userIds);

    void deleteGroupById(UUID objectId);

    void addUser(UUID objectId, String email);

    void deleteUser(UUID groupId, UUID userId);

    List<String> getUsersIdsFromGroup(UUID groupId);

    Boolean isUserInGroup(UUID groupId, UUID userId);

}
