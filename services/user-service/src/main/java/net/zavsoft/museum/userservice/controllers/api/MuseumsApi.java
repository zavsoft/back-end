package net.zavsoft.museum.userservice.controllers.api;

import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import net.zavsoft.museum.userservice.dtos.museum.FullMuseumInfoDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RequestMapping("/museum")
public interface MuseumsApi {

    @GetMapping("/current")
    FullInfoDto getCurrentUser();

    @PostMapping("/register")
    ResponseEntity<UUID> registerUser(@RequestBody UserRegistrationDto user);

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete")
    ResponseEntity<Void> deleteUser(@RequestParam UUID userId);

    @GetMapping("/id/{userId}")
    ResponseEntity<FullMuseumInfoDto> findById(@PathVariable("userId") UUID id);

    @GetMapping("/{username}")
    ResponseEntity<List<FullMuseumInfoDto>> findByUsername(@PathVariable("username") String username);

    @PutMapping("/avatar/{userId}")
    @PreAuthorize("hasRole('ADMIN') or #authorityService.hasPrimaryAuthority(#id)")
    ResponseEntity<Void> setAvatar(@PathVariable("userId") UUID id, @RequestParam("file") MultipartFile avatar)
        throws IOException;

    @GetMapping("/{userId}/avatar")
    ResponseEntity<String> getAvatar(@PathVariable UUID userId) throws IOException;

    @PreAuthorize("hasRole('ADMIN') or #authorityService.hasPrimaryAuthority(#user.getDto().getId())")
    @PutMapping("/update")
    ResponseEntity<Void> updateUser(@RequestBody FullMuseumInfoDto user);

    @PreAuthorize("hasRole('ADMIN') or #authorityService.hasPrimaryAuthority(#id)")
    @PutMapping("/{userId}/changeEmail")
    ResponseEntity<Void> changeEmail(@PathVariable("userId") UUID id, @RequestParam String email);

}
