package net.zavsoft.museum.userservice.services.impl;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.keycloaksecurity.Roles;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import net.zavsoft.museum.userservice.dtos.museum.FullMuseumInfoDto;
import net.zavsoft.museum.userservice.entities.Museum;
import net.zavsoft.museum.userservice.entities.User;
import net.zavsoft.museum.userservice.repos.MuseumRepository;
import net.zavsoft.museum.userservice.services.CommonUserService;
import net.zavsoft.museum.userservice.services.MuseumService;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

//todo get,update,delete services
@Slf4j
@Service
public class MuseumServiceImpl extends CommonUserService<Museum> implements MuseumService {


    private final GroupsResource groupsResource;

    private final GroupsServiceImpl groupsServiceImpl;

    public MuseumServiceImpl(UsersResource usersResource,
                             Keycloak keycloak,
                             RealmResource realmResource,
                             MuseumRepository museumRepository,
                             MinioFileStorageClient minio,
                             GroupsResource groupsResource,
                             GroupsServiceImpl groupsServiceImpl) {
        super(museumRepository, usersResource, keycloak, realmResource, minio);
        this.groupsResource = groupsResource;
        this.groupsServiceImpl = groupsServiceImpl;
    }

    @Override
    public List<FullMuseumInfoDto> findByUsername(String username) {
        Iterable<? extends User> iterable = findUserByUsername(username);
        return StreamSupport.stream(iterable.spliterator(), false)
                            .map(user -> new FullMuseumInfoDto(((Museum) user).toDto(),
                                                               usersResource.get(user.getId().toString()).toRepresentation(),
                                                               getAvatar(user.getId())))
                            .toList();

    }

    @Override
    public void updateUser(@Valid FullMuseumInfoDto userDto) {
        var user = (Museum) findUserById(userDto.getDto().getId());
        updateUserRepresentation(userDto.getDto().getId(), userDto.getKeyCloakDto());
        user.update(userDto.getDto());
        repository.save(user);
    }

    @Override
    public FullInfoDto getCurrentUser() {
        var id = UUID.fromString(SecurityContextHolder.getContext().getAuthentication().getName());
        return findById(id);
    }

    @Override
    protected void saveUser(UUID userId) {
        repository.save(new Museum(userId));
    }

    @Override
    public FullMuseumInfoDto findById(UUID id) {
        return new FullMuseumInfoDto(findUserById(id).toDto(),
                                     usersResource.get(id.toString()).toRepresentation(),
                                     getAvatar(id));
    }

    @Override
    public UUID addUser(@Valid UserRegistrationDto userReg, List<Roles> roles) {
        UUID newUser = super.addUser(userReg, roles);
        GroupRepresentation group = new GroupRepresentation();
        group.setName(newUser.toString());
        var resp = groupsResource.add(group);
        group.setId(CreatedResponseUtil.getCreatedId(resp));
        groupsServiceImpl.addUserToGroup(newUser, group);
        return newUser;
    }

}
