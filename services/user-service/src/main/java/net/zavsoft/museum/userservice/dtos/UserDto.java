package net.zavsoft.museum.userservice.dtos;

import java.util.UUID;

public interface UserDto {

    UUID getId();
}
