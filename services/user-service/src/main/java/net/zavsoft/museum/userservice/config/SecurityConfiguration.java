package net.zavsoft.museum.userservice.config;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.keycloaksecurity.KeycloakProperties;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final KeycloakProperties keycloakProperties;

    @Bean
    public Keycloak keycloak() {
        return KeycloakBuilder.builder()
                              .serverUrl(keycloakProperties.getServerUrl())
                              .realm(keycloakProperties.getRealm())
                              .clientId(keycloakProperties.getClientId())
                              .clientSecret(keycloakProperties.getClientSecret())
                              .grantType(OAuth2Constants.PASSWORD)
                              .username(keycloakProperties.getUsername())
                              .password(keycloakProperties.getPassword())
                              .build();
    }

    @Bean
    public RealmResource realmResource() {
        return keycloak().realm(keycloakProperties.getRealm());
    }

    @Bean
    public UsersResource usersResource() {
        return keycloak().realm(keycloakProperties.getRealm()).users();
    }

    @Bean
    public GroupsResource groupsResource() {
        return keycloak().realm(keycloakProperties.getRealm()).groups();
    }

}
