package net.zavsoft.museum.userservice.dtos;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.lang.Nullable;

import java.util.Objects;

@Getter
public abstract class FullUserInfoDto<T extends UserDto> implements FullInfoDto {

    @NotNull
    @JsonUnwrapped
    protected T dto;

    @NotNull
    @JsonUnwrapped
    protected KeyCloakUserInfoDto keyCloakDto;

    protected String avatarUrl;

    protected FullUserInfoDto(T dto, @Nullable UserRepresentation userRepresentation, String avatarUrl) {

        this.dto = dto;

        if (userRepresentation != null) {
            if (!Objects.equals(dto.getId().toString(), userRepresentation.getId())) {
                throw new IllegalArgumentException("Разные пользователи");
            }

            this.keyCloakDto = new KeyCloakUserInfoDto(userRepresentation.getUsername(),
                                                       userRepresentation.getFirstName(),
                                                       userRepresentation.getLastName(),
                                                       userRepresentation.getEmail(),
                                                       userRepresentation.isEmailVerified());
        }

        this.avatarUrl = avatarUrl;
    }

}
