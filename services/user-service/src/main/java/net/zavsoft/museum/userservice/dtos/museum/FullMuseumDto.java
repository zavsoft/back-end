package net.zavsoft.museum.userservice.dtos.museum;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.entities.MuseumAdmin;
import org.springframework.lang.Nullable;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class FullMuseumDto implements MuseumDto, FullInfoDto {

    @NotNull
    private final UUID id;

    @Nullable
    private final String avatarType;

    @Nullable
    private final UUID docId;

    @Nullable
    private final String museumName;

    @Nullable
    private final String country;

    @Nullable
    private final String city;

    @Nullable
    private final String address;

    @Nullable
    private final String description;

    @Nullable
    private final String site;

    @Nullable
    private final String directorName;

    @Nullable
    private final String phone;

    @Nullable
    private final MuseumAdmin museumAdmin;

    private final boolean verified;
}
