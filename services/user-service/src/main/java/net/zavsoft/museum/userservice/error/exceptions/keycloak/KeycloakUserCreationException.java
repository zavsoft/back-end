package net.zavsoft.museum.userservice.error.exceptions.keycloak;

import jakarta.ws.rs.core.Response;

public class KeycloakUserCreationException extends KeycloakOperationException {

    public KeycloakUserCreationException(Response.StatusType statusInfo) {
        super(statusInfo, "Keycloak насрал в штаны при создании пользователя");
    }
}
