package net.zavsoft.museum.userservice.controllers.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@RequestMapping("/internal")
public interface InternalApi {

    @GetMapping("/isUserInGroup")
    Boolean isUserInGroup(@RequestParam("groupId") UUID groupId, @RequestParam("userId") UUID userId);

}
