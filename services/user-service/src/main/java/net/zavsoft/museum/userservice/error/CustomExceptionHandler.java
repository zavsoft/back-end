package net.zavsoft.museum.userservice.error;

import jakarta.ws.rs.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.userservice.error.exceptions.keycloak.KeycloakOperationException;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler {

    @ExceptionHandler(KeycloakOperationException.class)
    public ResponseEntity<ErrorResponse> handleKeycloakUserCreationException(
        KeycloakOperationException e) {
        log.error(e.getMessage(), e);
        return ResponseEntity.internalServerError().body(
            ErrorResponse
                .builder(e, ProblemDetail
                                .forStatus(e.getStatusInfo().getStatusCode()))
                .build());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponse> handleNotFoundException(
        NotFoundException e) {
        log.error(e.getMessage(),e);
        return ResponseEntity.notFound().build();
    }
}


