package net.zavsoft.museum.userservice.dtos.user;

import net.zavsoft.museum.userservice.dtos.UserDto;

public interface SimpleUserDto extends UserDto {
}
