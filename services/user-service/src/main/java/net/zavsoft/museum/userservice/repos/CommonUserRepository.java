package net.zavsoft.museum.userservice.repos;

import java.util.UUID;
import net.zavsoft.museum.userservice.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CommonUserRepository<T extends User> extends CrudRepository<T, UUID> {
}
