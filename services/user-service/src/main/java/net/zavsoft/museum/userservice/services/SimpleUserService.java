package net.zavsoft.museum.userservice.services;

import jakarta.validation.Valid;
import java.util.List;
import java.util.UUID;
import net.zavsoft.museum.userservice.dtos.user.FullSimpleUserInfoDto;

public interface SimpleUserService extends UserService {

    List<FullSimpleUserInfoDto> findByUsername(String username);

    void updateUser(@Valid FullSimpleUserInfoDto userDto);

    FullSimpleUserInfoDto findById(UUID id);
}
