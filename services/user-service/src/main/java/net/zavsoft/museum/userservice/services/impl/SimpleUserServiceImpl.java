package net.zavsoft.museum.userservice.services.impl;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.user.FullSimpleUserInfoDto;
import net.zavsoft.museum.userservice.entities.SimpleUser;
import net.zavsoft.museum.userservice.entities.User;
import net.zavsoft.museum.userservice.repos.UserRepository;
import net.zavsoft.museum.userservice.services.CommonUserService;
import net.zavsoft.museum.userservice.services.SimpleUserService;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

//todo get,update,delete services
@Service
@Slf4j
public class SimpleUserServiceImpl extends CommonUserService<SimpleUser> implements SimpleUserService {


    public SimpleUserServiceImpl(UsersResource usersResource,
                                 Keycloak keycloak,
                                 RealmResource realmResource,
                                 UserRepository userRepository,
                                 MinioFileStorageClient minio) {
        super(userRepository, usersResource, keycloak, realmResource, minio);
    }

    @Override
    public List<FullSimpleUserInfoDto> findByUsername(String username) {
        Iterable<? extends User> iterable = findUserByUsername(username);
        return StreamSupport.stream(iterable.spliterator(), false)
                            .map(user -> new FullSimpleUserInfoDto(((SimpleUser) user).toDto(),
                                                                   usersResource.get(user.getId().toString()).toRepresentation(),
                                                                   getAvatar(user.getId())))
                            .toList();
    }

    @Override
    public void updateUser(@Valid FullSimpleUserInfoDto userDto) {
        var user = (SimpleUser) findUserById(userDto.getDto().getId());
        updateUserRepresentation(userDto.getDto().getId(), userDto.getKeyCloakDto());
        user.update(userDto.getDto());
        repository.save(user);
    }

    @Override
    public FullInfoDto getCurrentUser() {
        var id = UUID.fromString(SecurityContextHolder.getContext().getAuthentication().getName());
        return findById(id);
    }

    @Override
    protected void saveUser(UUID userId) {
        repository.save(new SimpleUser(userId));
    }

    @Override
    public FullSimpleUserInfoDto findById(UUID id) {
        return new FullSimpleUserInfoDto(findUserById(id).toDto(),
                                         usersResource.get(id.toString()).toRepresentation(),
                                         getAvatar(id));
    }

}
