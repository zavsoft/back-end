package net.zavsoft.museum.userservice.error.exceptions.keycloak;

import jakarta.ws.rs.core.Response;

public class KeycloakUserDeleteException extends KeycloakOperationException{

    public KeycloakUserDeleteException(Response.StatusType statusInfo) {
        super(statusInfo, "Ошибка Keycloak при попытке удаления пользователя");
    }

}
