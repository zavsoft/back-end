package net.zavsoft.museum.userservice.dtos;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Getter
@Setter
public class UserRegistrationDto {

    @NotNull
    private String username;

    @Nullable
    private String password;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    public UserRegistrationDto(String username, @Nullable String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
