package net.zavsoft.museum.userservice.controllers;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.userservice.controllers.api.UsersApi;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import net.zavsoft.museum.userservice.dtos.user.FullSimpleUserInfoDto;
import net.zavsoft.museum.userservice.services.SimpleUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UsersController implements UsersApi {

    private final SimpleUserService userService;

    @Override
    public ResponseEntity<UUID> registerUser(UserRegistrationDto user) {
        return ResponseEntity.ok(userService.addUser(user, List.of()));
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<FullSimpleUserInfoDto> findById(UUID id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @Override
    public ResponseEntity<List<FullSimpleUserInfoDto>> findByUsername(String username) {
        return ResponseEntity.ok(userService.findByUsername(username));
    }

    @Override
    public FullInfoDto getCurrentUser() {
        return userService.getCurrentUser();
    }

    @Override
    public ResponseEntity<Void> setAvatar(UUID id, MultipartFile avatar) throws IOException {
        userService.setAvatar(id, avatar);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updateUser(FullSimpleUserInfoDto user) {
        userService.updateUser(user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> changeEmail( UUID id, String email) {
        userService.changeEmail(id, email);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<String> getAvatar(@PathVariable UUID userId) {
        try {
            String url = userService.getAvatar(userId);
            return ResponseEntity.ok(url);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to generate temporary URL for file.");
        }
    }

}
