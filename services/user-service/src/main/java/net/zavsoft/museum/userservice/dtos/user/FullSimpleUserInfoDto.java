package net.zavsoft.museum.userservice.dtos.user;

import lombok.Getter;
import net.zavsoft.museum.userservice.dtos.FullUserInfoDto;
import org.keycloak.representations.idm.UserRepresentation;

@Getter
public class FullSimpleUserInfoDto extends FullUserInfoDto<FullSimpleUserDto> {

    public FullSimpleUserInfoDto(FullSimpleUserDto dto, UserRepresentation userRepresentation, String avatarUrl) {
        super(dto, userRepresentation, avatarUrl);
    }

}
