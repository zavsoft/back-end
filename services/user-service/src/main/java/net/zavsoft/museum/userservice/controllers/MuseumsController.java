package net.zavsoft.museum.userservice.controllers;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.keycloaksecurity.Roles;
import net.zavsoft.museum.userservice.controllers.api.MuseumsApi;
import net.zavsoft.museum.userservice.dtos.FullInfoDto;
import net.zavsoft.museum.userservice.dtos.UserRegistrationDto;
import net.zavsoft.museum.userservice.dtos.museum.FullMuseumInfoDto;
import net.zavsoft.museum.userservice.services.MuseumService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class MuseumsController implements MuseumsApi {

    private final MuseumService userService;

    @Override
    public ResponseEntity<UUID> registerUser(UserRegistrationDto user) {
        return ResponseEntity.ok(userService.addUser(user, List.of(Roles.MUSEUM)));
    }

    @Override
    public ResponseEntity<Void> deleteUser(UUID userId) {
        userService.deleteUser(userId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<FullMuseumInfoDto> findById(UUID id) {

        return ResponseEntity.ok(userService.findById(id));
    }

    @Override
    public ResponseEntity<List<FullMuseumInfoDto>> findByUsername(String username) {
        return ResponseEntity.ok(userService.findByUsername(username));
    }

    @Override
    public FullInfoDto getCurrentUser() {
        return userService.getCurrentUser();
    }

    @Override
    public ResponseEntity<Void> setAvatar(UUID id, MultipartFile avatar) throws IOException {
        userService.setAvatar(id, avatar);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updateUser(FullMuseumInfoDto user) {
        userService.updateUser(user);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> changeEmail(UUID id, String email) {
        userService.changeEmail(id, email);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<String> getAvatar(@PathVariable UUID userId) {
        try {
            String url = userService.getAvatar(userId);
            return ResponseEntity.ok(url);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to generate temporary URL for file.");
        }
    }

}
