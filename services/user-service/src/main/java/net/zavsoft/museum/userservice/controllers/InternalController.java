package net.zavsoft.museum.userservice.controllers;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.userservice.controllers.api.InternalApi;
import net.zavsoft.museum.userservice.services.GroupsService;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class InternalController implements InternalApi {

    private final GroupsService groupsService;

    @Override
    public Boolean isUserInGroup(UUID groupId, UUID userId) {
        return groupsService.isUserInGroup(groupId, userId);
    }

}
