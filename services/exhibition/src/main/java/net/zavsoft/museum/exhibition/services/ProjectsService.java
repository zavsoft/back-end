package net.zavsoft.museum.exhibition.services;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.zavsoft.museum.exception.ReceiveFileException;
import net.zavsoft.museum.exhibition.clients.GroupsClient;
import net.zavsoft.museum.exhibition.dto.ProjectDto;
import net.zavsoft.museum.exhibition.dto.ProjectUpdateDto;
import net.zavsoft.museum.exhibition.entities.Project;
import net.zavsoft.museum.exhibition.repositories.ProjectRepository;
import net.zavsoft.museum.keycloaksecurity.KeycloakUserDetailsUtils;
import net.zavsoft.museum.service.impl.MinioFileStorageClient;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectsService {

    private final ProjectRepository projectRepository;

    private final GroupsClient groupsClient;

    private final MinioFileStorageClient minio;

    public Project get(UUID projectId) {
        return projectRepository.findById(projectId).orElseThrow(() -> new RuntimeException("No project with id: " + projectId));
    }

    @SneakyThrows
    public ProjectDto getProject(UUID projectId) {
        var project = get(projectId);
        return new ProjectDto(project, new String(minio.find(project.getId(), "json").readAllBytes(), StandardCharsets.UTF_8));
    }

    @SneakyThrows
    public List<ProjectDto> get(Integer page, Integer pageSeize) {
        List<Project> projects = projectRepository.findAllByIsOpen(true, PageRequest.of(page, pageSeize));
        return projects.stream().map(project -> {
            try {
                return new ProjectDto(project,
                                      new String(minio.find(project.getId(), "json").readAllBytes(), StandardCharsets.UTF_8));
            } catch (IOException e) {
                throw new ReceiveFileException(e);
            }
        }).toList();
    }

    public UUID create(String name, String view) {
        var id = projectRepository.save(new Project(KeycloakUserDetailsUtils.getUserId(), name)).getId();
        minio.save(view.getBytes(), "json", id);
        groupsClient.addGroup(id);
        return id;
    }

    public void update(UUID projectId, ProjectUpdateDto dto) {
        Project project = get(projectId);
        project.setName(dto.name());
        project.setDetails(dto.details());
        project.setStartTime(dto.startTime());
        project.setEndTime(dto.endTime());
        projectRepository.save(project);
    }

    public void publishProject(UUID projectId) {
        Project project = get(projectId);
        project.setIsOpen(true);
        project.setPublishDate(ZonedDateTime.now());
        projectRepository.save(project);
    }

    public void delete(UUID projectId) {
        projectRepository.deleteById(projectId);
        if (minio.exist(projectId.toString())) {
            minio.delete(projectId.toString());
        }
    }

    public Optional<UUID> getOwnerId(UUID objectId) {
        return projectRepository.findById(objectId).map(Project::getOwnerId);
    }

    public void addView(UUID projectId, String view) {
        var project = get(projectId);
        minio.save(view.getBytes(), "json", project.getId());
    }

    public List<Project> getUsersProjects(Integer pageSize, Integer page) {
        var groups = groupsClient.getUsersGroup().stream().map(UUID::fromString).toList();
        return projectRepository.findAllByIds(groups, PageRequest.of(page, pageSize));
    }

}
