package net.zavsoft.museum.exhibition.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zavsoft.museum.exhibition.entities.Project;

@Getter
@AllArgsConstructor
public class ProjectDto {

    @JsonUnwrapped
    private Project project;

    private String data;

}
