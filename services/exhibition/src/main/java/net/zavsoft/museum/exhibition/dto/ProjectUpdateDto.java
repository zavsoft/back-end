package net.zavsoft.museum.exhibition.dto;

import java.time.ZonedDateTime;

public record ProjectUpdateDto(String name, String details,
                               ZonedDateTime startTime, ZonedDateTime endTime) {}
