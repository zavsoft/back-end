package net.zavsoft.museum.exhibition.entities;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@Entity
@NoArgsConstructor
public class Tag {

    @EmbeddedId
    @JsonUnwrapped
    private TagId id;

    public Tag(UUID projectId, UUID userId, String tag) {
        id = new TagId(projectId, userId, tag);
    }

}
