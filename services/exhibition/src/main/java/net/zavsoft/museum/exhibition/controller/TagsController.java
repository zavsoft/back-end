package net.zavsoft.museum.exhibition.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.exhibition.entities.Tag;
import net.zavsoft.museum.exhibition.services.TagService;
import net.zavsoft.museum.keycloaksecurity.KeycloakUserDetailsUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/tags")
public class TagsController {

    private final TagService tagService;

    @GetMapping("/")
    public ResponseEntity<List<Tag>> get(@RequestParam UUID projectId, @RequestParam UUID userId) {
        return ResponseEntity.ok(tagService.getTags(userId, projectId));
    }

    @Operation(summary = "Получает теги текущего пользователя в проекте")
    @GetMapping("/current")
    public ResponseEntity<List<Tag>> get(@RequestParam UUID projectId) {
        UUID userId = KeycloakUserDetailsUtils.getUserId();
        return ResponseEntity.ok(tagService.getTags(userId, projectId));
    }

    @PostMapping("/add")
    public ResponseEntity<Void> addTag(@RequestParam UUID projectId, @RequestParam UUID userId, @RequestParam String tag) {
        tagService.addTag(userId, projectId, tag);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteTag(@RequestParam UUID projectId, @RequestParam UUID userId, @RequestParam String tag) {
        tagService.deleteTag(userId, projectId, tag);
        return ResponseEntity.ok().build();
    }

}
