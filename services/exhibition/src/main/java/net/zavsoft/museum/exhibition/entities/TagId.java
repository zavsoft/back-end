package net.zavsoft.museum.exhibition.entities;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Embeddable
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TagId implements Serializable {

    @JoinColumn(table = "project", name = "id")
    private UUID projectId;

    private UUID userId;

    private String tag;

}
