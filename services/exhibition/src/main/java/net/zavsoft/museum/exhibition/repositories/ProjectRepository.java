package net.zavsoft.museum.exhibition.repositories;

import net.zavsoft.museum.exhibition.entities.Project;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepository extends CrudRepository<Project, UUID> {

    List<Project> findAllByIsOpen(Boolean isOpen, Pageable pageRequest);

    List<Project> findAll(Pageable pageRequest);

    @Query(value = "select p from Project p where p.id in :ids", countQuery = "select count(*) from Project p where p.id in :ids")
    List<Project> findAllByIds(List<UUID> ids, Pageable pageRequest);
}
