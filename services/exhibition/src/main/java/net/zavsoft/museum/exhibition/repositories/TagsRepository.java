package net.zavsoft.museum.exhibition.repositories;

import net.zavsoft.museum.exhibition.entities.Tag;
import net.zavsoft.museum.exhibition.entities.TagId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TagsRepository extends CrudRepository<Tag, TagId> {

    @Query("select t from Tag t where t.id.userId = :userId")
    List<Tag> findAllByUserId(UUID userId);

}
