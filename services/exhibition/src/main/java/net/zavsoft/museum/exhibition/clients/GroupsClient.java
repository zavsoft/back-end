package net.zavsoft.museum.exhibition.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.UUID;

@FeignClient(value = "userClient", url = "${services.auth}" + "/groups")
public interface GroupsClient {

    @GetMapping("/")
    List<String> getUsersGroup();

    @PostMapping("/create/{objectId}")
    void addGroup(@PathVariable UUID objectId);

}
