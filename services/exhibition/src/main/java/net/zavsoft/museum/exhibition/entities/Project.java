package net.zavsoft.museum.exhibition.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Entity
@NoArgsConstructor
public class Project {

    @Id
    @UuidGenerator
    private UUID id;

    private UUID ownerId;

    @Setter
    private String name;

    @Setter
    private String details;

    @Setter
    private Boolean isOpen;

    @Setter
    private ZonedDateTime publishDate;

    @Setter
    private ZonedDateTime startTime;

    @Setter
    private ZonedDateTime endTime;

    public Project(UUID ownerId, String name) {
        this.name = name;
        this.ownerId = ownerId;
        isOpen = false;
    }

}
