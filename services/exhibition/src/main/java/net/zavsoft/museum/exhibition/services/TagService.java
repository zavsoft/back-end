package net.zavsoft.museum.exhibition.services;

import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.exhibition.entities.Tag;
import net.zavsoft.museum.exhibition.entities.TagId;
import net.zavsoft.museum.exhibition.repositories.TagsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TagService {

    private final TagsRepository tagsRepository;

    public void addTag(UUID userId, UUID projectId, String tag) {
        tagsRepository.save(new Tag(projectId, userId, tag));
    }

    public void deleteTag(UUID userId, UUID projectId, String tag) {
        tagsRepository.deleteById(new TagId(projectId, userId, tag));
    }

    public List<Tag> getTags(UUID userId, UUID projectId) {
        return tagsRepository.findAllByUserId(userId);
    }

}
