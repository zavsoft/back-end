package net.zavsoft.museum.exhibition.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import net.zavsoft.museum.exhibition.dto.ProjectDto;
import net.zavsoft.museum.exhibition.dto.ProjectUpdateDto;
import net.zavsoft.museum.exhibition.entities.Project;
import net.zavsoft.museum.exhibition.services.ProjectsService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/projects")
public class ProjectController {

    private final ProjectsService projectsService;

    @Operation(summary = "Получение выставок по страницам")
    @GetMapping("/get")
    public ResponseEntity<List<ProjectDto>> get(@RequestParam Integer page, @RequestParam Integer pageSize) {
        return ResponseEntity.ok(projectsService.get(page, pageSize));
    }

    @Operation(summary = "Создание выставки")
    @PostMapping("/create")
    public ResponseEntity<UUID> create(@RequestParam String name, @RequestBody String view) {
        return ResponseEntity.ok(projectsService.create(name, view));
    }

    @Operation(summary = "Получение выставки по id")
    @GetMapping("/get/{projectId}")
    public ResponseEntity<ProjectDto> get(@PathVariable UUID projectId) {
        return ResponseEntity.ok(projectsService.getProject(projectId));
    }

    @Operation(summary = "Получение всех выставок текущего пользователя")
    @GetMapping("/users")
    public List<Project> getAllForUser(@RequestParam Integer page, @RequestParam Integer pageSize) {
        return projectsService.getUsersProjects(pageSize, page);
    }

    @GetMapping("/{projectId}/owner")
    public Optional<UUID> getOwnerId(@PathVariable UUID projectId) {
        return projectsService.getOwnerId(projectId);
    }

    @Operation(summary = "Удаление выставки по id")
    @DeleteMapping("/{projectId}/delete")
    public ResponseEntity<Void> delete(@PathVariable UUID projectId) {
        projectsService.delete(projectId);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Изменение данных созданной выставки")
    @PutMapping("/{projectId}/update")
    public ResponseEntity<Void> update(@PathVariable UUID projectId, @RequestBody ProjectUpdateDto dto) {
        projectsService.update(projectId, dto);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Добавление тела выставки")
    @PostMapping(value = "/view/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addView(@RequestBody String view, @RequestParam("projectId") UUID projectId) {
        projectsService.addView(projectId, view);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Публикация выставки")
    @PutMapping(value = "/{projectId}/publish")
    public ResponseEntity<Void> publishProject(@PathVariable UUID projectId) {
        projectsService.publishProject(projectId);
        return ResponseEntity.ok().build();
    }

}
